package ru.tsc.apozdnov.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(final String message) {
        super(message);
    }

    public AbstractException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    protected AbstractException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
