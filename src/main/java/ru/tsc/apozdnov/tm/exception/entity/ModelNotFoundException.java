package ru.tsc.apozdnov.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Fault!!! Model not found!!!");
    }

}
