package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(final String userId, final String name);

    Project create(final String userId, String name, String description);

}
