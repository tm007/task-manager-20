package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, RoleType role);

    List<User> findAll();

    User findOneById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User remove(User user);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
