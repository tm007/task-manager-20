package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommand();

    void add(AbstractCommand abstractCommand);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String arg);

}
