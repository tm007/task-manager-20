package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    List<M> findAll();

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M removeById(String id);

    M removeByIndex(Integer index);

    M remove(M model);

    M add(M model);

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

    void removeAll(Collection<M> collection);

}
