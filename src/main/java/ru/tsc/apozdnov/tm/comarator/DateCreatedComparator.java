package ru.tsc.apozdnov.tm.comarator;

import ru.tsc.apozdnov.tm.api.model.IHasDateCreated;

import java.util.Comparator;

public enum DateCreatedComparator implements Comparator<IHasDateCreated> {

    INSTANCE;

    @Override
    public int compare(final IHasDateCreated o1, final IHasDateCreated o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateCreated() == null || o2.getDateCreated() == null) return 0;
        return o1.getDateCreated().compareTo(o2.getDateCreated());
    }
}
