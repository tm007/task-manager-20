package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** COMPLETE PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
    }

}
